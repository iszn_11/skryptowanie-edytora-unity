# Na początek
Jeżeli ktoś chce się dowiedzieć, co można zrobić z edytorem, polecam obejrzeć
[Editor Scripting for n00bs](https://github.com/roboryantron/UnityEditorJunkie).
Jest z 2016 roku i nie wszystko jest idealne, ale pokazuje wiele, m.in. Gizmos, CustomEditor, ReorderableList, EditorWindow, Handles, InitializeOnLoad i ScriptableObject. Jeżeli ktoś
chce pisać skrypty edytora, a nie tylko wiedzieć, jakie są możliwości, polecam znaleźć inne poradniki, np.
[Editor Scripting](https://learn.unity.com/tutorial/editor-scripting).

Jeżeli ktoś nie wie, czym jest ScriptableObject lub nie zna dobrego zastosowania, polecam film
[Game Architecture with Scriptable Objects](https://www.youtube.com/watch?v=raQ3iHhE_Kk).
Jest to zagadnienie bardziej architektoniczne, niż edytorowe, bo można używać ScriptableObject nie pisząc ani jednej linii skryptu edytora. Dla zainteresowanych jest jeszcze 
[Overthrowing the MonoBehaviour Tyranny in a Glorious Scriptable Object Revolution](https://www.youtube.com/watch?v=6vmRwLYWNRo).
Jeżeli kogoś interesuje kod źródłowy, niech zajrzy najpierw do
[Architect your code in Unity in the smart way with Scriptable Objects](https://unity3d.com/how-to/architect-with-scriptable-objects)
na blogu Unity, gdzie przykład FloatVariable z pierwszego filmu jest trochę lepiej napisany. Reszta kodu jest w repozytoriach podlinkowanych w opisach filmów.

# Inspektory i edytory
Większość skryptów edytora polega na pisaniu własnych inspektorów i edytorów, które rysują coś na scenie. Dokumentacja tłumaczy wiele edytorowych kwestii dość dobrze (nie wszystkie),
trzeba tylko wiedzieć, gdzie szukać. Ze stron
[Editor Windows](https://docs.unity3d.com/Manual/editor-EditorWindows.html),
[Property Drawers](https://docs.unity3d.com/Manual/editor-PropertyDrawers.html)
i
[Custom Editors](https://docs.unity3d.com/Manual/editor-CustomEditors.html)
można dowiedzieć się o trzech najważniejszych klasach, jeśli chodzi o inspektory i edytory. Dodatkowo można zajrzeć na:
- [Going deep with IMGUI and Editor Customization](https://blogs.unity3d.com/2015/12/22/going-deep-with-imgui-and-editor-customization/)
- [Mastering UnityEditor Handles](https://connect.unity.com/p/mastering-unityeditor-handles)
- [Curves and Splines](https://catlikecoding.com/unity/tutorials/curves-and-splines/) (stary poradnik, ale działający i nie wymaga prawie żadnych poprawek)

Gdy pisze się cokolwiek dla edytora Unity, to im więcej silnika znamy, tym lepiej dla nas. Wiedząc, co istnieje w Unity, można pisać lepsze skrypty. Warto wiedzieć np. o istnieniu: 

- [PostProcessSceneAttribute](https://docs.unity3d.com/ScriptReference/Callbacks.PostProcessSceneAttribute.html)
- [InitializeOnLoadAttribute](https://docs.unity3d.com/ScriptReference/InitializeOnLoadAttribute.html)
- [RuntimeInitializeOnLoadMethodAttribute](https://docs.unity3d.com/ScriptReference/RuntimeInitializeOnLoadMethodAttribute.html)

# Serializacja i polimorfizm
Serializacja to kolejne zagadnienie nieedytorowe, ale praktycznie wymagane, gdy piszemy inspektory. Wpis na blogu Unity
[Serialization in Unity](https://blogs.unity3d.com/2014/06/24/serialization-in-unity/)
dość dobrze podsumowuje wszystko, co da się zrobić i czego nie da się zrobić. Dużo można się dowiedzieć, szukając obejść dla braków w systemie serializacji, np. serializacji typów
generycznych, serializacji polimorficznej i wsparcia dla Dictionary. Można np. używać własnych klas generycznych do serializacji, ale
[trzeba tworzyć niegeneryczne podklasy](http://answers.unity.com/answers/462070/view.html).
W tym samym pytaniu można znaleźć
[przykład serializowalnego słownika](http://answers.unity.com/answers/809221/view.html),
który używa [ISerializationCallbackReceiver](https://docs.unity3d.com/ScriptReference/ISerializationCallbackReceiver.html)
(w dokumentacji zresztą jest prawie ten słownik). Można też np.
[napisać całą implementację](https://forum.unity.com/threads/finally-a-serializable-dictionary-for-unity-extracted-from-system-collections-generic.335797/).
Potencjalne rozwiązanie problemu z polimorficzną serializacją można znaleźć w filmie z 2014 roku
[Unite 2014 - The Hack Spectrum: Tips, Tricks and Hacks for Unity](https://www.youtube.com/watch?v=SyR4OYZpVqQ).
Pierwsza część (Polymorphic Serialization) jest nadal aktualna i polecam ją bardzo, bo porusza mniej znane tematy.

# Przykłady inspektorów
Zamiast pisać samemu, możemy użyć istniejących.
[UnityEditorJunkie](https://github.com/roboryantron/UnityEditorJunkie)
oraz
[NaughtyAttributes](https://github.com/dbrizov/NaughtyAttributes)
są na licencji MIT i zawierają dużo pomocnych atrybutów, dzięki którym można uniknąć pisania własnych edytorów. Można też przeczytać kod i dowiedzieć się, jak to się robi. Jako
ciekawostkę podam projekt
[VFW](https://github.com/vexe/VFW),
który próbował przepisać serializację na taką, która potrafi wszystko. Autor jednak z tego pomysłu zrezygnował
([wypowiedź](https://github.com/vexe/VFW/issues/88#issuecomment-213857086)) i wyleczył się z obiektówki.
