using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class PolyEmbedDrawer<BaseType> : PropertyDrawer {

	private GUIStyle popupStyle;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		if (popupStyle == null) {
			popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions")) {
				imagePosition = ImagePosition.ImageOnly
			};
		}

		label = EditorGUI.BeginProperty(position, label, property);
		position = EditorGUI.PrefixLabel(position, label);

		SerializedProperty link = property.FindPropertyRelative("link");
		SerializedProperty reference = property.FindPropertyRelative("reference");

		Rect buttonRect = new Rect(position);
		buttonRect.yMin += popupStyle.margin.top;
		buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
		position.xMin = buttonRect.xMax;

		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		Type[] subtypes = ReflectionUtility.GetSubtypes<BaseType>();
		List<string> popupOptions = new List<string> { "(Link)" };
		popupOptions.AddRange(subtypes.Select(t => t.Name));

		int oldValue = 0;
		if (!link.boolValue) {
			oldValue = Array.FindIndex(
				subtypes,
				t => t == reference.objectReferenceValue.GetType()
			) + 1;
		}
		int result = EditorGUI.Popup(buttonRect, oldValue, popupOptions.ToArray(), popupStyle);

		if (result != oldValue) {
			link.boolValue = result == 0;
			if (oldValue != 0) {
				UnityEngine.Object toRemove = reference.objectReferenceValue;
				AssetDatabase.RemoveObjectFromAsset(toRemove);
				reference.objectReferenceValue = null;
			}
			if (result != 0) {
				ScriptableObject toInsert = ScriptableObject.CreateInstance(subtypes[result - 1]);
				toInsert.name = subtypes[result - 1].Name;
				AssetDatabase.AddObjectToAsset(toInsert, property.serializedObject.targetObject);
				reference.objectReferenceValue = toInsert;
			}
			AssetDatabase.SaveAssets();
		}

		GUI.enabled = link.boolValue;
		EditorGUI.PropertyField(position, reference, GUIContent.none);

		EditorGUI.indentLevel = indent;
		EditorGUI.EndProperty();
	}

}

[CustomPropertyDrawer(typeof(PolyEmbedAction))]
public class PolyEmbedActionDrawer : PolyEmbedDrawer<Action> {}
