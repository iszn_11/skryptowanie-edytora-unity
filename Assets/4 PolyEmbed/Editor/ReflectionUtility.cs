﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class ReflectionUtility {

	public static Type[] GetSubtypes(Type type, bool includeAbstract = false) {
		List<Type> ret = new List<Type>();

		foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
			ret.AddRange(assembly
				.GetTypes()
				.Where(t => t.IsSubclassOf(type)
					&& (includeAbstract || !t.IsAbstract)));
		}
		return ret.ToArray();
	}

	public static Type[] GetSubtypes<T>(bool includeAbstract = false) {
		return GetSubtypes(typeof(T), includeAbstract);
	}

}
