﻿using UnityEngine;

public abstract class PolyEmbed<SOType> where SOType : ScriptableObject {

	public bool link = true;
	public SOType reference;

	public static implicit operator SOType(PolyEmbed<SOType> embed) => embed.reference;
}
