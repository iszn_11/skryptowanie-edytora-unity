﻿using UnityEngine;

[CreateAssetMenu(menuName = "Action/Debug")]
public class ActionDebug : Action {

	public string message;

	public override void Do() {
		Debug.Log(message);
	}

}
