using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Action/Event")]
public class ActionEvent : Action {

	public UnityEvent m_event;

	public override void Do() {
		m_event.Invoke();
	}

}
