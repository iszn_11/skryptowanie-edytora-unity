﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(FloatRef))]
public class FloatRefDrawer : PropertyDrawer {

	private readonly string[] PopupOptions = { "Constant", "Variable" };

	private GUIStyle popupStyle;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		if (popupStyle == null) {
			popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions")) {
				imagePosition = ImagePosition.ImageOnly
			};
		}

		label = EditorGUI.BeginProperty(position, label, property);
		position = EditorGUI.PrefixLabel(position, label);

		SerializedProperty constant = property.FindPropertyRelative("constant");
		SerializedProperty constantValue = property.FindPropertyRelative("constantValue");
		SerializedProperty variable = property.FindPropertyRelative("variable");

		Rect buttonRect = new Rect(position);
		buttonRect.yMin += popupStyle.margin.top;
		buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
		position.xMin = buttonRect.xMax;

		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		int result = EditorGUI.Popup(buttonRect, constant.boolValue ? 0 : 1, PopupOptions, popupStyle);
		constant.boolValue = result == 0;

		EditorGUI.PropertyField(position, constant.boolValue ? constantValue : variable, GUIContent.none);

		EditorGUI.indentLevel = indent;
		EditorGUI.EndProperty();
	}

}
