﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class FloatVar : ScriptableObject, ISerializationCallbackReceiver {
	public float initialValue;

	[NonSerialized]
	public float runtimeValue;

	public void OnAfterDeserialize() {
		runtimeValue = initialValue;
	}

	public void OnBeforeSerialize() {}
}
