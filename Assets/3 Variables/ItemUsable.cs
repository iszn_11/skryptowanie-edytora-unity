﻿using UnityEngine;

[CreateAssetMenu]
public class ItemUsable : Item {

	public PolyEmbedAction action;

	[ContextMenu("Use")]
	public void Use() {
		action.reference.Do();
	}

}
