﻿using UnityEngine;

[System.Serializable]
public class FloatRef {
	public bool constant = true;
	public float constantValue;
	public FloatVar variable;

	public FloatRef() {}

	public FloatRef(float value) {
		constantValue = value;
	}

	public float Value => constant ? constantValue : variable.runtimeValue;
	public static implicit operator float(FloatRef reference) => reference.Value;
}
