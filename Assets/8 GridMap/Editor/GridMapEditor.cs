﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[InitializeOnLoad]
public static class GridMapEditor {

	private static readonly Vector3 GRID_SIZE = new Vector3(1.0f, 0.5f, 1.0f);

	private static GameObject prefab;
	private static bool cursorOnGrid = false;
	private static Vector2Int cursorGridPos;

	private static Vector3 GridWorldPosition => new Vector3(
		(cursorGridPos.x + 0.5f) * GRID_SIZE.x,
		0.0f,
		(cursorGridPos.y + 0.5f) * GRID_SIZE.z);

	static GridMapEditor() {
		ResetEditor();
	}

	[MenuItem("GridMap/Reset Editor")]
	private static void ResetEditor() {
		SceneView.duringSceneGui -= OnSceneGUI; // onSceneGUIDelegate
		SceneView.duringSceneGui += OnSceneGUI;
		Camera.onPreCull -= OnPreCull;
		Camera.onPreCull += OnPreCull;
		prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/8 GridMap/Cube.prefab");
	}

	private static void OnPreCull(Camera camera) {
		if (cursorOnGrid) {
			MeshFilter meshFilter = prefab.GetComponent<MeshFilter>();
			MeshRenderer meshRenderer = prefab.GetComponent<MeshRenderer>();
			Transform meshTransform = prefab.transform;

			Mesh mesh = meshFilter.sharedMesh;
			Material[] materials = meshRenderer.sharedMaterials;

			for (int i = 0; i < mesh.subMeshCount; i++) {
				Graphics.DrawMesh(
					mesh,
					Matrix4x4.Translate(GridWorldPosition) * meshTransform.localToWorldMatrix,
					materials[i], prefab.layer, camera, i);
			}
		}
	}

	private static void OnSceneGUI(SceneView sceneView) {
		if (EditorSceneManager.GetActiveScene().buildIndex != 0) return;

		Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
		Plane gridPlane = new Plane(Vector3.up, Vector3.zero);

		if (gridPlane.Raycast(ray, out var dist)) {
			Vector3 point = ray.origin + ray.direction.normalized * dist;
			cursorOnGrid = true;
			cursorGridPos = new Vector2Int(
				Mathf.FloorToInt(point.x / GRID_SIZE.x),
				Mathf.FloorToInt(point.z / GRID_SIZE.z));
		}
		else {
			cursorOnGrid = false;
		}

		if (cursorOnGrid && Event.current.type == EventType.MouseDown && Event.current.button == 0) {
			GameObject go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
			go.transform.position = GridWorldPosition;
			Event.current.Use();
		}

		SceneView.RepaintAll();
	}

}
