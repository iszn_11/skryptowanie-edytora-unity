#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[AddComponentMenu("Utilities/Box Random")]
public class BoxRandom : MonoBehaviour {

	public Vector3 Get {
		get {
			Vector3 p = new Vector3(
				Random.value - 0.5f,
				Random.value - 0.5f,
				Random.value - 0.5f);
			return transform.TransformPoint(p);
		}
	}

	private void OnDrawGizmosSelected() {
		Gizmos.color = Color.magenta;
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
	}

	[ContextMenu("Generate to Console")]
	private void GenerateToConsole() {
		Debug.Log(Get);
	}

	#if UNITY_EDITOR
	[MenuItem("GameObject/Box Random", false, 0)]
	private static void CreateBoxRandom(MenuCommand menuCommand) {
		GameObject go = new GameObject("Box Random", typeof(BoxRandom));
		GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
		Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
		Selection.activeObject = go;
	}
	#endif

}
