using UnityEngine;

[DisallowMultipleComponent, RequireComponent(typeof(Rigidbody))]
public class NPC : MonoBehaviour {
	[SerializeField]
	private new string name;
	[SerializeField, TextArea(4, 10)]
	private string description;

	[SerializeField, Header("Stats")]
	private int level = 1;

	[SerializeField, Space(10)]
	private int HP = 1;
	[SerializeField]
	private int maxHP = 1;

	[SerializeField, Space(10)]
	private Stats stats;

	[SerializeField, Range(0, 1), Space(10), Tooltip("Probability of dealing triple damage")]
	private float critChance = 0.5f;

	/*private new Rigidbody rigidbody;

	private void Awake() {
		rigidbody = GetComponent<Rigidbody>();
	}*/

	[SerializeField, HideInInspector]
	private new Rigidbody rigidbody;

	private void Reset() {
		rigidbody = GetComponent<Rigidbody>();
	}

	private void OnValidate() {
		maxHP = Mathf.Max(maxHP, 1);
		HP = Mathf.Clamp(HP, 1, maxHP);
	}
}
