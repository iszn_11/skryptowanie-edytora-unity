[System.Serializable]
public class Stats {
	public int strength;
	public int agility;
	public int stamina;
	public int charm;
	public int luck;
}
