﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Stats))]
public class StatsDrawer : PropertyDrawer {

	private const float LABEL_WIDTH = 32;
	private const float HSPACING = 4;
	private const float VSPACING = 2;

	private const int WIDTH = 3;
	private const int HEIGHT = 2;

	private Rect rect;
	private float fieldWidth;
	private float hOffset;

	private Rect LabelRect(int x, int y) {
		float vOffset = EditorGUIUtility.singleLineHeight + VSPACING;
		return new Rect(
			rect.x + x * hOffset,
			rect.y + y * vOffset,
			LABEL_WIDTH,
			EditorGUIUtility.singleLineHeight
		);
	}

	private Rect FieldRect(int x, int y) {
		float vOffset = EditorGUIUtility.singleLineHeight + VSPACING;
		return new Rect(
			rect.x + x * hOffset + LABEL_WIDTH,
			rect.y + y * vOffset,
			fieldWidth,
			EditorGUIUtility.singleLineHeight
		);
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		return HEIGHT * EditorGUIUtility.singleLineHeight + (HEIGHT - 1) * VSPACING;
	}

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		this.rect = position;
		fieldWidth = (position.width - WIDTH * LABEL_WIDTH - (WIDTH - 1) * HSPACING) / WIDTH;
		hOffset = LABEL_WIDTH + fieldWidth + HSPACING;

		EditorGUI.BeginProperty(position, label, property);

		EditorGUI.LabelField(LabelRect(0, 0), new GUIContent("STR", "Strength"));
		EditorGUI.PropertyField(FieldRect(0, 0), property.FindPropertyRelative("strength"), GUIContent.none);

		EditorGUI.LabelField(LabelRect(1, 0), new GUIContent("AGL", "Agility"));
		EditorGUI.PropertyField(FieldRect(1, 0), property.FindPropertyRelative("agility"), GUIContent.none);

		EditorGUI.LabelField(LabelRect(2, 0), new GUIContent("STM", "Stamina"));
		EditorGUI.PropertyField(FieldRect(2, 0), property.FindPropertyRelative("stamina"), GUIContent.none);

		EditorGUI.LabelField(LabelRect(0, 1), new GUIContent("CHR", "Charm"));
		EditorGUI.PropertyField(FieldRect(0, 1), property.FindPropertyRelative("charm"), GUIContent.none);

		EditorGUI.LabelField(LabelRect(1, 1), new GUIContent("LCK", "Luck"));
		EditorGUI.PropertyField(FieldRect(1, 1), property.FindPropertyRelative("luck"), GUIContent.none);

		EditorGUI.EndProperty();
    }

}
