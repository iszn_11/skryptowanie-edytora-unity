﻿using UnityEngine;

public class BezierCurve : MonoBehaviour {
	public Vector3 p0;
	public Vector3 p1;
	public Vector3 p2;
	public Vector3 p3;

	public Vector3 Get(float t) {
		float t2 = t * t;
		float t3 = t2 * t;
		float tinv = 1.0f - t;
		float tinv2 = tinv * tinv;
		float tinv3 = tinv2 * tinv;

		return transform.TransformPoint(tinv3 * p0 +
			3.0f * tinv2 * t * p1 +
			3.0f * tinv * t2 * p2 +
			t3 * p3);
	}
}
