using System.Collections.Generic;
using UnityEngine;

public class BezierSpline : MonoBehaviour {

	[SerializeField]
	private List<Vector3> points;

	public Vector3 GetPoint(float t) {
		int index = 0;
		if (t >= 1.0f) {
			t = 1.0f;
			index = points.Count - 4;
		}
		else {
			t = Mathf.Clamp01(t) * CurveCount;
			int curveIndex = (int)t;
			t %= 1.0f;
			index = 3 * curveIndex;
		}

		float t1 = 1.0f - t;
		return t1 * t1 * t1 * points[index] +
			3.0f * t1 * t1 * t * points[index + 1] +
			3.0f * t1 * t * t * points[index + 2] +
			t * t * t * points[index + 3];
	}

	public int CurveCount => (points.Count - 1) / 3;

	public int ControlPointCount => points.Count;
	public Vector3 GetControlPoint(int index) => points[index];

	public void SetControlPoint(int index, Vector3 point) {
		if (index % 3 == 0) {
			Vector3 d = point - points[index];
			if (index > 0) points[index - 1] += d;
			if (index < points.Count - 1) points[index + 1] += d;
		}
		points[index] = point;
	}

	private void Reset() {
		points = new List<Vector3>() {
			new Vector3(1.0f, 0.0f, 0.0f),
			new Vector3(2.0f, 0.0f, 0.0f),
			new Vector3(3.0f, 0.0f, 0.0f),
			new Vector3(4.0f, 0.0f, 0.0f)
		};
	}

	public void AddCurve() {
		Vector3 p = points[points.Count - 1];
		p.x += 1.0f;
		points.Add(p);
		p.x += 1.0f;
		points.Add(p);
		p.x += 1.0f;
		points.Add(p);
	}

}
