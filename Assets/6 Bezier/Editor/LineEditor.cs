﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Line))]
public class LineEditor : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();
	}

	private void OnSceneGUI() {
		Line line = target as Line;
		Transform transform = line.transform;
		Quaternion rotation =
			Tools.pivotRotation == PivotRotation.Local
			? transform.rotation
			: Quaternion.identity;

		SerializedProperty p0prop = serializedObject.FindProperty("p0");
		SerializedProperty p1prop = serializedObject.FindProperty("p1");

		Vector3 p0 = transform.TransformPoint(line.p0);
		Vector3 p1 = transform.TransformPoint(line.p1);

		Handles.color = Color.white;
		Handles.DrawLine(p0, p1);

		EditorGUI.BeginChangeCheck();
		p0 = Handles.PositionHandle(p0, rotation);
		if (EditorGUI.EndChangeCheck()) {
			p0prop.vector3Value = transform.InverseTransformPoint(p0);
		}

		EditorGUI.BeginChangeCheck();
		p1 = Handles.PositionHandle(p1, rotation);
		if (EditorGUI.EndChangeCheck()) {
			p1prop.vector3Value = transform.InverseTransformPoint(p1);
		}

		serializedObject.ApplyModifiedProperties();
	}
}
