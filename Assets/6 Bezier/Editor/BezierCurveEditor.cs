using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BezierCurve))]
public class BezierCurveEditor : Editor {

	private void OnSceneGUI() {
		BezierCurve curve = target as BezierCurve;
		Transform transform = curve.transform;
		Quaternion rotation =
			Tools.pivotRotation == PivotRotation.Local
			? transform.rotation
			: Quaternion.identity;

		SerializedProperty[] props = new SerializedProperty[4];
		Vector3[] p = new Vector3[4];

		for (int i = 0; i < 4; i++) {
			props[i] = serializedObject.FindProperty($"p{i}");
			p[i] = transform.TransformPoint(props[i].vector3Value);

			EditorGUI.BeginChangeCheck();
			p[i] = Handles.PositionHandle(p[i], rotation);
			if (EditorGUI.EndChangeCheck()) {
				props[i].vector3Value = transform.InverseTransformPoint(p[i]);
			}
		}

		Handles.color = Color.green;
		Handles.DrawLine(p[0], p[1]);
		Handles.DrawLine(p[3], p[2]);

		Handles.DrawBezier(p[0], p[3], p[1], p[2], Color.white, null, 2.0f);

		serializedObject.ApplyModifiedProperties();
	}
}
