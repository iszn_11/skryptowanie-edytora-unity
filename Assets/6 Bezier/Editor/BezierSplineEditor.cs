using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BezierSpline))]
public class BezierSplineEditor : Editor {

	private const float HandleSize = 0.04f;
	private const float PickSize = 0.06f;

	private BezierSpline spline;
	private Transform handleTransform;
	private Quaternion handleRotation;
	private int selectedIndex = -1;

	public override void OnInspectorGUI() {
		spline = (BezierSpline)target;

		if (selectedIndex >= 0 && selectedIndex < spline.ControlPointCount) {
			GUILayout.Label("Selected Point");

			EditorGUI.BeginChangeCheck();
			Vector3 point = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(selectedIndex));
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(spline, "Move Point");
				spline.SetControlPoint(selectedIndex, point);
				SceneView.RepaintAll();
			}
		}

		if (GUILayout.Button("Add Curve")) {
			Undo.RecordObject(spline, "Add Curve");
			spline.AddCurve();
			SceneView.RepaintAll();
		}
	}

	private void OnSceneGUI() {
		spline = (BezierSpline)target;

		handleTransform = spline.transform;
		handleRotation = Tools.pivotRotation == PivotRotation.Local ?
			handleTransform.rotation : Quaternion.identity;

		Vector3 p0 = ShowPoint(0);
		for (int i = 1; i < spline.ControlPointCount; i += 3) {
			Vector3 p1 = ShowPoint(i);
			Vector3 p2 = ShowPoint(i + 1);
			Vector3 p3 = ShowPoint(i + 2);

			Handles.color = Color.gray;
			Handles.DrawLine(p0, p1);
			Handles.DrawLine(p2, p3);

			Handles.DrawBezier(p0, p3, p1, p2, Color.white, null, 2.0f);

			p0 = p3;
		}
	}

	private Vector3 ShowPoint(int index) {
		Vector3 p = handleTransform.TransformPoint(spline.GetControlPoint(index));
		float size = HandleUtility.GetHandleSize(p);

		Handles.color = Color.white;

		if (Handles.Button(
			p,
			handleRotation,
			size * HandleSize,
			size * PickSize,
			Handles.DotHandleCap)) {
			selectedIndex = index;
			Repaint();
		}

		if (selectedIndex == index) {
			EditorGUI.BeginChangeCheck();
			p = Handles.PositionHandle(p, handleRotation);
			if (EditorGUI.EndChangeCheck()) {
				Undo.RecordObject(spline, "Move Point");
				spline.SetControlPoint(index, handleTransform.InverseTransformPoint(p));
			}
		}

		return p;
	}

}
