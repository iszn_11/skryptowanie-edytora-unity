using UnityEngine;

public class BezierDemo : MonoBehaviour
{
	public Vector3 p0;
	public Vector3 p1;
	public Vector3 p2;
	[Range(0.0f, 1.0f)]
	public float x;

	private void OnDrawGizmosSelected() {
		Gizmos.DrawLine(p0, p1);
		Gizmos.DrawLine(p1, p2);

		Vector3 p3 = Vector3.Lerp(p0, p1, x);
		Vector3 p4 = Vector3.Lerp(p1, p2, x);

		Gizmos.color = Color.blue;
		Gizmos.DrawLine(p3, p4);

		Vector3 p5 = Vector3.Lerp(p3, p4, x);
		Gizmos.DrawSphere(p5, 0.1f);
	}
}
