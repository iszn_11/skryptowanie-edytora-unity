using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class ScriptableSetEditor<SOType> : Editor where SOType : ScriptableObject {

	private ReorderableList list;

	private void OnEnable() {
		if (target == null) return;
		SOType set = target as SOType;

		list = new ReorderableList(
			serializedObject,
			serializedObject.FindProperty("list"),
			true,
			false,
			true,
			true
		);

		list.drawElementCallback = (rect, index, isActive, isFocued) => {
			const float RenameWidth = 52;
			const float OpenWidth = 40;

			SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
			UnityEngine.Object objectRef = element.objectReferenceValue;

			EditorGUI.LabelField(
				new Rect(
					rect.x,
					rect.y + 2,
					rect.width - RenameWidth - OpenWidth - 2,
					EditorGUIUtility.singleLineHeight
				), objectRef.name
			);

			Rect buttonRect = new Rect(
				rect.xMax - RenameWidth - OpenWidth,
				rect.y + 2,
				RenameWidth,
				EditorGUIUtility.singleLineHeight
			);

			if (GUI.Button(buttonRect, "Rename", EditorStyles.miniButtonLeft)) {
				PopupWindow.Show(buttonRect, new StringPopup((newName) => {
					objectRef.name = newName;

					serializedObject.ApplyModifiedProperties();
					AssetDatabase.SaveAssets();
				}, "Rename", objectRef.name));
			}

			buttonRect = new Rect(
				rect.xMax - OpenWidth,
				rect.y + 2,
				OpenWidth,
				EditorGUIUtility.singleLineHeight
			);

			if (GUI.Button(buttonRect, "Open", EditorStyles.miniButtonRight)) {
				AssetDatabase.OpenAsset(objectRef);
			}
		};

		list.onAddDropdownCallback = (rect, list) => {
			List<Type> types = new List<Type>() {
				set.GetType().BaseType.GenericTypeArguments[0]
			};
			types.AddRange(ReflectionUtility.GetSubtypes(types[0]));

			GenericMenu menu = new GenericMenu();
			foreach (var type in types) {
				menu.AddItem(new GUIContent(type.Name), false, () => {
					PopupWindow.Show(rect, new StringPopup((newName) => {
						int index = list.serializedProperty.arraySize;
						list.serializedProperty.arraySize++;
						list.index = index;

						SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);

						ScriptableObject subObject = CreateInstance(type);
						subObject.name = newName;
						AssetDatabase.AddObjectToAsset(subObject, target);
						element.objectReferenceValue = subObject;

						serializedObject.ApplyModifiedProperties();
						AssetDatabase.SaveAssets();
					}, "Add", type.Name));
				});
			}
			menu.ShowAsContext();
		};

		list.onRemoveCallback = (list) => {
			SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(list.index);

			AssetDatabase.RemoveObjectFromAsset(element.objectReferenceValue);

			element.objectReferenceValue = null;
			list.serializedProperty.DeleteArrayElementAtIndex(list.index);

			serializedObject.ApplyModifiedProperties();
			AssetDatabase.SaveAssets();
		};
	}

	public override void OnInspectorGUI() {
		list.DoLayoutList();
	}

}

[CustomEditor(typeof(ItemSet))]
public class ItemSetEditor : ScriptableSetEditor<ItemSet> {}
