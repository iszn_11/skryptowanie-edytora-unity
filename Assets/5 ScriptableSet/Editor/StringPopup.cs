﻿using System;
using UnityEditor;
using UnityEngine;

public class StringPopup : PopupWindowContent {

	private Action<string> callback;
	private string buttonName;
	private string text;

	public StringPopup(Action<string> callback, string buttonName, string text = "") {
		this.callback = callback;
		this.buttonName = buttonName;
		this.text = text;
	}

	public override Vector2 GetWindowSize() => new Vector2(160, 40);

	public override void OnOpen() {
		base.OnOpen();
		editorWindow.Focus();
	}

	public override void OnGUI(Rect rect) {
		text = EditorGUI.TextField(new Rect(2, 2, 156, 16), text);
		if (GUI.Button(new Rect(2, 22, 156, 16), buttonName)) {
			callback(text);
			editorWindow.Close();
		}
	}

}
