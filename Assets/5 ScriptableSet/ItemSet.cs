﻿using UnityEngine;

[CreateAssetMenu()]
public class ItemSet : ScriptableSet<Item> {}
