﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ScriptableSet<SOType> : ScriptableObject where SOType : ScriptableObject {

	[SerializeField]
	private List<SOType> list;

	public IReadOnlyList<SOType> List => list;

}
