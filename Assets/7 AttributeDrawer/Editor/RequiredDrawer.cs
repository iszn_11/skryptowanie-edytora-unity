﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(RequiredAttribute))]
public class RequiredDrawer : PropertyDrawer {

	private const float BOX_HEIGHT = 40.0f;

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		float height = base.GetPropertyHeight(property, label);

		if (property.propertyType != SerializedPropertyType.ObjectReference) return height;
		if (property.objectReferenceValue == null) {
			height += EditorGUIUtility.standardVerticalSpacing + BOX_HEIGHT;
		}

		return height;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		if (property.propertyType != SerializedPropertyType.ObjectReference) {
			base.OnGUI(position, property, label);
			return;
		}

		if (property.objectReferenceValue == null) {
			Rect rect = new Rect(position);
			rect.height = BOX_HEIGHT;

			EditorGUI.HelpBox(rect, $"{property.displayName} is required.", MessageType.Warning);

			position.yMin = rect.yMax + EditorGUIUtility.standardVerticalSpacing;
		}
		EditorGUI.PropertyField(position, property);
	}
}
